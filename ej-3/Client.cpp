#include <list>
#include <iostream>
using namespace std;
#include "Client.h"

Client::Client(){}

Client::Client(string nombre, string telefono, int saldo, bool moroso){
	this->nombre = nombre;
	this->telefono = telefono;
	this->saldo = saldo;
	this->moroso = moroso;
}

string Client::get_nombre(){
	return this->nombre;
}
string Client::get_telefono(){
	return this->telefono;
}
int Client::get_saldo(){
	return this->saldo;
}
bool Client::get_moroso(){
	return this->moroso;
}
	
void Client::set_nombre(string nombre){
	this->nombre = nombre;
}
void Client::set_telefono(string telefono){
	this->telefono = telefono;
}
void Client::set_saldo(int saldo){
	this->saldo = saldo;
}
void Client::set_moroso(bool moroso){
	this->moroso = moroso;
}
