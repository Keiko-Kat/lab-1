#include <list>
#include <iostream>
using namespace std;
#include "Client.cpp"

void print_it(Client array[], int N){
	int i;
	for (i = 0; i < N; i++){
		cout<<endl;
		cout<<"~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~"<<endl;		
		cout<<"El cliente "<<array[i].get_nombre()<<", cuyo numero es: '"<<array[i].get_telefono()<<"'"<<endl;
		cout<<"tiene un saldo de: "<<array[i].get_saldo();
		if (array[i].get_moroso() == true){
			cout<<" y es moroso."<<endl;
		}else{
			cout<<" y no es moroso";
		}
		cout<<"~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~"<<endl;	
		cout<<endl;
		cout<<endl;
	}
}
	
int main(){
	int quantity, temp;
	string in;
	bool mors;
	
	cout<<"Ingrese tamaño del arreglo: ";
	getline(cin, in);
	quantity = stoi(in);
	if(quantity <= 0){
		cout<<"Cantidad invalida, ingrese de nuevo"<<endl;
		cout<<"Tamaño: ";
		getline(cin, in);
		quantity = stoi(in);
	}
	
	Client array[quantity];
	
	int i;
	for (i = 0; i < quantity; i++){
		cout<<"Ingrese nombre del cliente n°["<<i+1<<"]"<<endl;
		cout<<"~~~~> ";
		getline(cin, in);
		array[i].set_nombre(in);
		cout<<endl;
		cout<<"Ingrese telefono del cliente n°["<<i+1<<"]"<<endl;
		cout<<"~~~~> ";
		getline(cin, in);
		array[i].set_telefono(in);
		cout<<endl;
		cout<<"Ingrese saldo del cliente n°["<<i+1<<"]"<<endl;
		cout<<"~~~~> ";
		getline(cin, in);
		temp = stoi(in);
		array[i].set_saldo(temp);
		cout<<endl;
		cout<<"¿Es el cliente n°["<<i+1<<"] moroso?"<<endl;
		cout<<"Ingrese 1 si lo es, cualquier otra letra si no"<<endl;
		cout<<"~~~~> ";
		getline(cin, in);
		temp = stoi(in);
		if (temp == 1){
			mors = true;
		}else{
			mors = false;
		}
		array[i].set_moroso(mors);
	}
	cout<<endl;
	print_it(array, quantity);
	
}
		
	
