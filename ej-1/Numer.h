#include <list>
#include <iostream>
using namespace std;

class Num{
	private:
		int num = 0;
		int square = 0;
	public:
		Num();
		Num(int num);
		/* métodos get and set */
		int get_num();
		int get_square();
		void set_num(int num);
};
