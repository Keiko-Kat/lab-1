#include <list>
#include <iostream>
using namespace std;

class Client{
	public:
		string nombre;
		string telefono;
		int saldo;
		bool moroso;
	private:
		Client();
		Client(string nombre, string telefono, int saldo, bool moroso);
		
		string get_nombre();
		string get_telefono();
		int get_saldo();
		bool get_moroso();
		
		void set_nombre(string nombre);
		void set_telefono(string telefono);
		void set_saldo(int saldo);
		void set_moroso(bool moroso);
};		
