#include <list>
#include <iostream>
using namespace std;
#include "ejer-1.h"
#include "Numer.cpp"


int sumar(Num arrg[], int num){
	int i = 0, sum = 0;
	for (i = 0; i < num; i++){
		sum = sum + arrg[i].get_square();
	}
	return sum;
}

int main(){
	int quantity, temp, finish;
	string in;
	
	cout<<"Ingrese tamaño del arreglo: ";
	getline(cin, in);
	quantity = stoi(in);
	if(quantity <= 0){
		cout<<"Cantidad invalida, ingrese de nuevo"<<endl;
		cout<<"Tamaño: ";
		getline(cin, in);
		quantity = stoi(in);
	}
	
	Num arrg[quantity];
	
	for(int i = 0; i < quantity; i++){
		
		cout<<"Ingrese numero en posicion ["<<i<<"]: ";
		getline(cin, in);
		temp = stoi(in);
		arrg[i].set_num(temp);
	}	
	
	finish = sumar(arrg, quantity);
	
	cout<<"El resultado final es: "<<finish<<endl;
	
	
	return 0;
}
